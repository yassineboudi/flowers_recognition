# Flowers Recognition

In this app, flower detection can be done done based on deep neural network. The types of the flower used in this app are: daisy, dandelion, rose, sunflower and tulip

### Prerequisites

OS tested:

- Ubuntu 16.04

Dependencies:

- pytorch 1.3.1

## Usage

The program has one modes:

1. detect: given a an image or directory of images as input, returns the type of the flower

```
python app.py <command> [args]
```

### Detect command

```
python app.py detect <input_image>
```

#### Input(s):

- `<input_image>` : path to the input image or directory of images.

#### Outputs:

- `<result>` : Type of the flower.

#### Example:

```
python app.py detect test/8_rose.jpg
```

### Test

Just launch the following command

```
pytest
```

## Dataset

The data used for this app is available at: https://www.kaggle.com/alxmamaev/flowers-recognition

## Refrences

https://pytorch.org
https://www.kaggle.com

## Authors

Yassine Boudi
