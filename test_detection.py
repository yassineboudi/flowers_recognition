#!usr/bin/python

from detectionapi import DetectionApi
import pytest
import warnings

warnings.filterwarnings("ignore")


def test_detect_flower():

    img_test_path = "test/8_rose.jpg"
    detect_flower = DetectionApi(img_test_path)

    assert detect_flower.predict()["name"] == "rose"
